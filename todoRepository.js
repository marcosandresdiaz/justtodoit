const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TodoItem = new Schema({
  id: Number,
  text: String,
  done: Boolean,
  date: String
});

const Todo = mongoose.model('Todo', new Schema({
  title: String,
  description: String,
  todos: [TodoItem],
  userId: String,
  visitedByUserIds: [String]
}));

module.exports = {
  async findRecent(userId) {
    return await Todo.find({"visitedByUserIds": userId});
  },
  async create(title, userId) {
    const todo = {
      title,
      description: "An example description",
      todos: [
        {
          id: 1,
          text: "Do the dishes",
          done: false,
          date: null
        },
        {
          id: 2,
          text: "Become a millionaire",
          done: false,
          date: new Date().toISOString().substring(0, 10)
        },
      ],
      userId
    };
    return await Todo.create(todo);
  },
  async update(_id, updatedTodo) {
    await Todo.updateOne({ _id },
      updatedTodo
    ).exec();
  },
  async findById(_id) {
    if (mongoose.Types.ObjectId.isValid(_id)) {
      return await Todo.findById(_id).exec();
    } else {
      return null;
    }
  },
  async delete(_id, userId) {
    await Todo.deleteOne({
      _id,
      userId
    }).exec();
  },
  async markAsVisited(_id, userId) {
    await Todo.updateOne({_id}, { $addToSet: { "visitedByUserIds": userId }})
  }
}