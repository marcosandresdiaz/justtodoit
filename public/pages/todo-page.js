Vue.component("todo-page", {
  props: ["todo-id"],
  data() {
    return {
      editing: false,
      todoList: null,
      notFound: false
    }
  },
  mounted() {
    axios.get(`/api/todos/${this.todoId}`)
      .then(response => {
        this.todoList = response.data;
      }).catch(err => {
        if (err.response.status === 404) {
          this.notFound = true;
        }
      })
  },
  methods: {
    toggleTodo(todo) {
      todo.done = !todo.done;
      this.save();
    },
    update(editedTodoList) {
      this.todoList.title = editedTodoList.title;
      this.todoList.description = editedTodoList.description;
      this.todoList.todos = editedTodoList.todos;
      this.save();
      this.editing = false;
    },
    save() {
      axios.put(`/api/todos/${this.todoId}`, this.todoList)
        .then(response => {
          console.log("Saved");
        });
    }
  },
  template: `
        <v-row>
          <v-col offset-md="3" sm="6" offset="0" cols="12">
            <template v-if="todoList">
              <todo-view v-if="!editing" :todo-list="todoList" @edit="editing = true" @toggle-todo="toggleTodo">
              </todo-view>
              <todo-edit v-else :original-todo-list="todoList" @save="update">
              </todo-edit>
            </template>
            <div v-if="notFound" class="text-center">
              <v-icon x-large>mdi-emoticon-confused-outline</v-icon>
              <h1 class="text-center"> Oops! To-do list not found</h1>
            </div>
          </v-col>
        </v-row>
  `
})