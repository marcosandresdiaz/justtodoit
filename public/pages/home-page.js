Vue.component("home-page", {
  data() {
    return {
      recentTodos: [],
      dialog: false,
      title: ""
    }
  },
  created() {
    axios.get("/api/todos/recent")
      .then(response => {
        this.recentTodos = response.data;
      })
  },
  methods: {
    create() {
      axios.post("/api/todos", {
        title: this.title
      })
        .then(response => {
          location.href = location.href + '?' + response.data._id;
          this.dialog = false;
        });
    }
  },
  template: `
    <div>
      <h1 class="text-center mt-10">
        <transition appear name="fade">
          <v-icon class="main-logo" style="transition: all 1s" color="green">
            mdi-checkbox-marked-circle-outline
          </v-icon>
        </transition>
      </h1>
      <h1 class="text-h2 text-center mt-10">Simple to-do lists.</h1>
      <h1 class="text-center mt-4 mb-10" :class="recentTodos.length > 0 ? 'text-h4' : 'text-h2'">Let's 
        <v-dialog
          v-model="dialog"
          width="500"
        >
          <template v-slot:activator="{ on, attrs }">
            <a v-on="on" v-bind="attrs" class="green--text">make one!</a>
          </template>
          <form @submit.prevent="create">
            <v-card>
              <v-card-text>
                <v-text-field color="green" label="Title" v-model="title" required minlength="3" maxlength="200" autocomplete="off"></v-text-field>
              </v-card-text>
              <v-divider></v-divider>
              <v-card-actions>
                <v-spacer></v-spacer>
                <v-btn color="secondary" text @click="dialog = false">Cancel</v-btn>
                <v-btn color="green" text type="submit">Create</v-btn>
              </v-card-actions>
            </v-card>
          </form>
        </v-dialog>
      </h1>
      <template v-if="recentTodos.length">
        <v-row>
          <v-col cols="6" cols-sm="12" offset="3">
            <v-card elevation="2">
              <v-card-title>
                <v-icon
                >
                  mdi-playlist-check
                </v-icon>&nbsp;
                Recent lists
              </v-card-title>
              <v-card-text>
                <ul>
                  <li v-for="todo in recentTodos">
                    <a :href="'?' + todo._id">{{todo.title}}</a>
                  </li>
                </ul>
              </v-card-text>
            </v-card>
          </v-col>
        </v-row>
      </template>
    </div>
  `
})