Vue.component("app-bar", {
  template: `
  <v-app-bar dark color="green">
    <v-toolbar-title>
      <a href="/" class="text-decoration-none white--text">
        <h2 class="font-weight-light">
          <v-icon large>
            mdi-check
        </v-icon>
        &nbsp;justtodoit
        </h2>
      </a>
    </v-toolbar-title>
    <v-spacer></v-spacer>
  </v-app-bar>
  `
});