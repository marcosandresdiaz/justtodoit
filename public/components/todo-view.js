Vue.component("todo-view", {
  props: {
    todoList: {
      required: true,
      type: Object
    }
  },
  methods: {
    toggleTodo(todo) {
      this.$emit("toggle-todo", todo);
    },
    todoDateColorClass(todo) {
      return daysRemainingFromToday(todo.date) < 0 ? 'grey--text text--lighten-2' : [];
    },
    edit() {
      this.$emit("edit");
    }
  },
  template: `
    <div>
      <v-row justify="space-between">
        <h1 class="pt-2 pb-1">{{todoList.title}}</h1>
        <v-btn fab @click="edit">
          <v-icon>mdi-pencil</v-icon>
        </v-btn>
      </v-row>
      <v-row>
        <p class="pt-2 pb-1">{{todoList.description}}</p>
      </v-row>
      <v-row>
        <todo-progress-bar :todos="todoList.todos"/>
      </v-row>
      <div v-for="todo in todoList.todos" :key="todo.id">
        <v-row class="pl-1" justify="space-between" dense>
          <v-checkbox class="pb-1" color="green" :label="todo.text" hide-details :input-value="todo.done" @click="toggleTodo(todo)">
          </v-checkbox>
          <span :class="todoDateColorClass(todo)" class="pt-5">
            {{todo.date | dateAndRemainingTime}}
          </span>
        </v-row>
      </div>
    </div>
  `
})