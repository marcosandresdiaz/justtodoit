Vue.component("todo-progress-bar", {
  props: {
    todos: {
      required: true,
      type: Array
    }
  },
  computed: {
    progress() {
      return 100 * this.todos.filter(todo => todo.done).length / this.todos.length;
    },
  },
  template: `
  <v-progress-linear :value="progress" :color="progress >= 100 ? 'green lighten-2' : 'green'" height="10">
  </v-progress-linear>
  `
})