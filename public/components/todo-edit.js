function insert(array, element, index) {
  return array.slice(0, index)
    .concat([element])
    .concat(array.slice(index));
}

Vue.component("todo-edit", {
  props: {
    originalTodoList: {
      required: true,
      type: Object
    },
  },
  data() {
    return {
      todoList: {
        title: this.originalTodoList.title,
        description: this.originalTodoList.description,
        todos: this.originalTodoList.todos,
        menu: false
      },
      dragging: {
        started: false,
        originalIndex: null,
        destinationIndex: null,
        dropped: true
      },
      confirmDeleteDialogOpen: false,
      deleteError: "",
    }
  },
  methods: {
    blur(todo) {
      if (todo.text === "") {
        this.remove(todo);
      }
    },
    save() {
      this.$emit("save", this.todoList);
    },
    handleEnter(todo) {
      if (todo.text.length > 0) {
        const currentIndex = this.todoList.todos.indexOf(todo);
        const newTodo = {
          id: Date.now(),
          text: "",
          done: false,
          date: null
        };
        this.insertAt(currentIndex + 1, newTodo);
      }
    },
    insertAt(index, todo) {
      this.todoList.todos = insert(this.todoList.todos, todo, index);
    },
    handleDelete(todo) {
      if (this.todoList.todos.length > 1) {
        if (todo.text === "") {
          this.remove(todo);
        }
      }
    },
    remove(todo) {
      this.todoList.todos.splice(this.todoList.todos.indexOf(todo), 1);
    },
    drag(index) {
      this.dragging.started = true;
      this.dragging.originalIndex = index;
      this.dragging.destinationIndex = index;
    },
    dragEnd() {
      this.dragging.started = false;
    },
    drop() {
      this.dragging.started = false;
      this.todoList.todos = this.draggingPreviewTodos;
    },
    selectDragDestination(index) {
      this.dragging.destinationIndex = index;
    },
    unselectDragDestination() {
      this.dragging.destinationIndex = this.dragging.originalIndex;
    },
    deleteTodo() {
      axios.delete(`/api/todos/${this.originalTodoList._id}`)
        .then(() => {
          location.href = "/";
          this.confirmDeleteDialogOpen = false;
          this.deleteError = "";
        }).catch(err => {
          this.deleteError = err.response.data;
        })
    }
  },
  computed: {
    draggingPreviewTodos() {
      const draggedTodo = this.todoList.todos[this.dragging.originalIndex];
      const copy = this.todoList.todos.slice();
      copy.splice(this.dragging.originalIndex, 1);
      return insert(copy, draggedTodo, this.dragging.destinationIndex);
    }
  },
  directives: {
    focus: {
      inserted: function (el) {
        el.querySelector("input").focus();
      }
    }
  },
  template: `
  <div v-else>
    <v-row justify="space-between">
      <v-text-field v-model="todoList.title" hide-details style="font-size: 2em; font-weight:bold"
        maxlength="100" @keydown.enter="save">
      </v-text-field>
      <v-btn fab @click="save" class="ml-3">
        <v-icon>mdi-floppy</v-icon>
      </v-btn>
    </v-row>
    <v-row>
      <v-textarea dense class="pt-1 pb-4" rows="1" auto-grow v-model="todoList.description" hide-details>
      </v-textarea>
    </v-row>
    <v-row>
      <todo-progress-bar :todos="todoList.todos"/>
    </v-row>
    <template v-if="!dragging.started">
      <div v-for="(todo, index) in todoList.todos" :key="todo.id">
        <v-row dense draggable @drag="drag(index)">
          <v-checkbox v-model="todo.done" hide-details class="shrink mt-4 ml-1"></v-checkbox>
          <v-text-field v-model="todo.text" hide-details @keydown.backspace.delete="handleDelete(todo)"
            @keydown.enter="handleEnter(todo)" v-focus @blur="blur(todo)">
          </v-text-field>
          <v-menu :nudge-right="40" transition="scale-transition" offset-y min-width="290px">
            <template v-slot:activator="{ on, attrs }">
              <span class="mt-5 ml-2 grey--text" v-bind="attrs" v-on="on">
                <v-icon color="grey">mdi-calendar</v-icon> {{todo.date | dateAndRemainingTime}}
              </span>
            </template>
            <v-date-picker v-model="todo.date">
              <v-btn text @click="menu = false; todo.date = null;">clear</v-btn>
            </v-date-picker>
          </v-menu>
        </v-row>
      </div>
    </template>
    <template v-else>
      <div v-for="(todo, index) in draggingPreviewTodos" :key="todo.id">
        <v-row dense @dragover.prevent="selectDragDestination(index)" @dragend="dragEnd" @drop.prevent="drop">
          <v-checkbox :input-value="todo.done" hide-details class="shrink mt-4 ml-1"></v-checkbox>
          <v-text-field :value="todo.text" hide-details></v-text-field>
          <span class="mt-5 ml-2 grey--text">
            <v-icon color="grey">mdi-calendar</v-icon> {{todo.date | dateAndRemainingTime}}
          </span>
        </v-row>
      </div>
    </template>
    <v-divider></v-divider>
    <v-row class="mt-5">
      <v-dialog
          v-model="confirmDeleteDialogOpen"
          max-width="500"
        >
        <v-card>
          <v-card-title class="headline">
            Are you sure you want to delete <br>'{{todoList.title}}'?
          </v-card-title>
          <v-card-text><p class="red--text" v-if="deleteError"><span v-html="deleteError"></span></p></v-card-text>
          <v-card-actions>
            <v-spacer></v-spacer>
            <v-btn
              color="secondary"
              text
              @click="confirmDeleteDialogOpen = false"
            >
              Cancel
            </v-btn>
            <v-btn
              color="red darken-1"
              text
              @click="deleteTodo"
            >
              Delete
            </v-btn>
          </v-card-actions>
        </v-card>
        <template v-slot:activator="{ on, attrs }">
          <v-btn v-on="on" v-bind="attrs" color="red" text >Delete</v-btn>
        </template>
      </v-dialog>
    </v-row>
  </div>
  `
})