const express = require('express')
const cookieSession = require('cookie-session')
const bodyParser = require('body-parser')
const uuidv4 = require('uuid').v4;
const mongoose = require('mongoose');
const todoRepository = require('./todoRepository');

const app = express()
const MAX_AGE_DAY = 24 * 60 * 60 * 1000;

app.use(express.static('public'));
app.use(bodyParser.json());

app.use(cookieSession({
  signed: false,
  maxAge: MAX_AGE_DAY * 365 * 10
}));

app.use(function (req, res, next) {
  if (!req.session.id) {
    req.session.id = uuidv4();
  }
  next()
});


app.get('/api/todos/recent', async (req, res) => {
  res.send(await todoRepository.findRecent(req.session.id));
});

app.get('/api/todos/:id', async (req, res) => {
  const todo = await todoRepository.findById(req.params.id);
  if (todo) {
    todoRepository.markAsVisited(req.params.id, req.session.id);
    res.send(todo);
  } else {
    res.status(404);
    res.send(null);
  }
});

app.post('/api/todos', async (req, res) => {
  const createdTodo = await todoRepository.create(req.body.title, req.session.id);
  res.send(createdTodo);
});

app.put('/api/todos/:id', async (req, res) => {
  res.send(await todoRepository.update(req.params.id, req.body));
});

app.delete('/api/todos/:id', async (req, res) => {
  res.send(await todoRepository.delete(req.params.id, req.session.id));
});

const mongoConnectionString = process.env.MONGO_CONNECTION_STRING || 'mongodb://localhost/justtodoit'
const port = process.env.PORT || 3000

mongoose.connect(mongoConnectionString, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true
}).then((err) => {
  app.listen(port, () => {
    console.log(`Started at http://localhost:${port}`);
  });
});
